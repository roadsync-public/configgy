import json
import os
import boto3
import pytest
from moto import mock_ssm, mock_secretsmanager
from roadsync_configgy.cli import main
from roadsync_configgy import load_config
from unittest.mock import patch
import sys
from ruamel.yaml import YAML

@pytest.fixture
def mock_cli_aws_resources():
    with mock_ssm(), mock_secretsmanager():
        # Mock SSM parameter
        ssm_client = boto3.client('ssm', region_name='us-east-1')
        ssm_client.put_parameter(Name='/cli_test_param', Value='cli_test_value', Type='String')

        # Mock Secret in Secrets Manager
        secrets_manager_client = boto3.client('secretsmanager', region_name='us-east-1')
        secret_name = "cli_test_secret"
        secrets_manager_client.create_secret(Name=secret_name, SecretString='cli_test_secret_value')

        yield


def test_cli_materialize_json(tmp_path, mock_cli_aws_resources):
    # Create a mock JSON config file with AWS URIs
    config_content = json.dumps({
        "ssm_value": "ssm://cli_test_param",
        "secret_value": "secret://cli_test_secret"
    })
    config_file = tmp_path / "cli_config.json"
    config_file.write_text(config_content)

    output_file = tmp_path / "materialized_config.json"

    # Run the CLI command using patch to simulate command line args
    with patch('sys.argv', ["configgy", "materialize", str(config_file), str(output_file)]):
        main()

    # Verify the content of the materialized output
    with open(output_file, "r") as result_file:
        result_data = json.load(result_file)
        assert result_data['ssm_value'] == 'cli_test_value', "SSM value should be resolved correctly in CLI"
        assert result_data['secret_value'] == 'cli_test_secret_value', "Secret value should be resolved correctly in CLI"


def test_cli_materialize_yaml(tmp_path, mock_cli_aws_resources):
    from ruamel.yaml import YAML
    yaml = YAML()

    # Create a mock YAML config file with AWS URIs
    config_content = """
    ssm_value: ssm://cli_test_param
    secret_value: secret://cli_test_secret
    """
    config_file = tmp_path / "cli_config.yaml"
    config_file.write_text(config_content)

    output_file = tmp_path / "materialized_config.yaml"

    # Run the CLI command using patch to simulate command line args
    with patch('sys.argv', ["configgy", "materialize", str(config_file), str(output_file)]):
        main()

    # Verify the content of the materialized output
    with open(output_file, "r") as result_file:
        materialized_data = yaml.load(result_file)
        assert materialized_data['ssm_value'] == 'cli_test_value', "SSM value should be resolved correctly in YAML"
        assert materialized_data['secret_value'] == 'cli_test_secret_value', "Secret value should be resolved correctly in YAML"


def test_cli_missing_file(tmp_path):
    # Attempt to run the CLI with a non-existent file
    missing_file = tmp_path / "non_existent.json"
    output_file = tmp_path / "materialized_config.json"

    with patch('sys.argv', ["configgy", "materialize", str(missing_file), str(output_file)]):
        with pytest.raises(SystemExit) as e:
            main()
        assert e.value.code == 1, "CLI should exit with error code 1 for missing file"


def test_cli_invalid_command(tmp_path):
    # Attempt to run the CLI with an invalid command
    config_file = tmp_path / "cli_config.json"
    output_file = tmp_path / "materialized_config.json"

    with patch('sys.argv', ["configgy", "invalid_command", str(config_file), str(output_file)]):
        with pytest.raises(SystemExit) as e:
            main()
        assert e.value.code == 2, "CLI should exit with error code 2 for invalid commands"

@pytest.fixture
def mock_cli_aws_resources_with_json():
    with mock_ssm(), mock_secretsmanager():
        ssm_client = boto3.client('ssm', region_name='us-east-1')
        ssm_client.put_parameter(Name='/cli_test_json', Value='{"key": "value"}', Type='String')

        secrets_manager_client = boto3.client('secretsmanager', region_name='us-east-1')
        secret_name = "cli_test_json_secret"
        secrets_manager_client.create_secret(Name=secret_name, SecretString='{"secret_key": "secret_value"}')

        yield


def test_cli_materialize_with_env_var(tmp_path):
    os.environ["CLI_TEST_ENV_VAR"] = "env_value"
    
    # Create a config with an environment variable reference
    config_content = json.dumps({
        "env_value": "env://CLI_TEST_ENV_VAR"
    })
    config_file = tmp_path / "cli_env_config.json"
    config_file.write_text(config_content)

    output_file = tmp_path / "materialized_env_config.json"

    with patch('sys.argv', ["configgy", "materialize", str(config_file), str(output_file)]):
        main()

    with open(output_file, "r") as result_file:
        result_data = json.load(result_file)
        assert result_data['env_value'] == 'env_value', "Environment variable should be resolved correctly in CLI"


def test_cli_materialize_with_json_ssm(tmp_path, mock_cli_aws_resources_with_json):
    # Create a config with a JSON SSM reference
    config_content = json.dumps({
        "json_ssm_value": "json+ssm://cli_test_json"
    })
    config_file = tmp_path / "cli_json_ssm_config.json"
    config_file.write_text(config_content)

    output_file = tmp_path / "materialized_json_ssm_config.json"

    with patch('sys.argv', ["configgy", "materialize", str(config_file), str(output_file)]):
        main()

    with open(output_file, "r") as result_file:
        result_data = json.load(result_file)
        assert result_data['json_ssm_value']['key'] == 'value', "JSON SSM value should be correctly parsed in CLI"


def test_cli_materialize_with_invalid_json(tmp_path):
    # Create a config file with invalid JSON content
    invalid_config_content = '{"key": "value",'
    config_file = tmp_path / "invalid_config.json"
    config_file.write_text(invalid_config_content)

    output_file = tmp_path / "invalid_materialized_config.json"

    with patch('sys.argv', ["configgy", "materialize", str(config_file), str(output_file)]):
        with pytest.raises(SystemExit) as e:
            main()
        assert e.value.code == 1, "CLI should exit with error code 1 for invalid JSON content"

def test_cli_help_message(capsys):
    with pytest.raises(SystemExit) as e:
        with patch('sys.argv', ["configgy", "--help"]):
            main()
    captured = capsys.readouterr()
    assert "usage: configgy" in captured.out, "CLI help message should be displayed"
    assert e.value.code == 0, "Help message should not raise an error"

def test_cli_materialize_json(tmp_path):
    # Create a valid JSON config file
    config_content = json.dumps({"key": "value"})
    json_file = tmp_path / "config.json"
    json_file.write_text(config_content)

    output_file = tmp_path / "output.json"

    with patch('sys.argv', ["configgy", "materialize", str(json_file), str(output_file)]):
        main()

    with open(output_file, "r") as result_file:
        result_data = json.load(result_file)
        assert result_data['key'] == 'value', "JSON file should be processed correctly"

def test_cli_materialize_yaml(tmp_path):
    yaml = YAML()
    yaml_file = tmp_path / "config.yaml"
    yaml_content = """
    key: value
    """
    yaml_file.write_text(yaml_content)

    output_file = tmp_path / "output.yaml"

    with patch('sys.argv', ["configgy", "materialize", str(yaml_file), str(output_file)]):
        main()

    with open(output_file, "r") as result_file:
        loaded_yaml = yaml.load(result_file)
        assert loaded_yaml['key'] == 'value', "YAML file should be processed correctly"


def test_cli_materialize_yml(tmp_path):
    yaml = YAML()
    yml_file = tmp_path / "config.yml"
    yml_content = """
    key: value
    """
    yml_file.write_text(yml_content)

    output_file = tmp_path / "output.yml"

    with patch('sys.argv', ["configgy", "materialize", str(yml_file), str(output_file)]):
        main()

    with open(output_file, "r") as result_file:
        loaded_yaml = yaml.load(result_file)
        assert loaded_yaml['key'] == 'value', "YML file should be processed correctly"


def test_cli_invalid_file_extension(tmp_path):
    invalid_file = tmp_path / "config.txt"
    invalid_file.write_text("key: value")

    output_file = tmp_path / "output.txt"

    with patch('sys.argv', ["configgy", "materialize", str(invalid_file), str(output_file)]):
        with pytest.raises(SystemExit) as e:
            main()
        assert e.value.code == 1, "Invalid file extension should cause CLI to exit with error code 1"