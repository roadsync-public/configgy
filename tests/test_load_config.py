import json
import os
import boto3
import pytest
from moto import mock_ssm, mock_secretsmanager, mock_s3
from roadsync_configgy import load_config 
from box.exceptions import BoxError

@pytest.fixture
def mock_aws_resources():
    with mock_ssm(), mock_secretsmanager():
        # Mock SSM parameter
        ssm_client = boto3.client('ssm', region_name='us-east-1')
        ssm_client.put_parameter(Name='/test_param', Value='test_value', Type='String')

        # Mock Secret in Secrets Manager
        secrets_manager_client = boto3.client('secretsmanager', region_name='us-east-1')
        secret_name = "test_secret"
        secrets_manager_client.create_secret(Name=secret_name, SecretString='test_secret_value')

        yield

@pytest.fixture
def mock_aws_resources_with_invalid_json():
    with mock_ssm(), mock_secretsmanager():
        # Set up existing mocks as before
        ssm_client = boto3.client('ssm', region_name='us-east-1')
        ssm_client.put_parameter(Name='test_param', Value='test_value', Type='String')

        secrets_manager_client = boto3.client('secretsmanager', region_name='us-east-1')
        secret_name = "test_secret"
        secrets_manager_client.create_secret(Name=secret_name, SecretString='test_secret_value')

        # Mock an SSM parameter with invalid JSON content
        ssm_client.put_parameter(Name='/invalid_json_param', Value='{"invalid_json"', Type='String')

        yield

@pytest.fixture
def mock_non_recursive_references():
    with mock_ssm():
        ssm_client = boto3.client('ssm', region_name='us-east-1')

        # Mock SSM parameters without creating actual circular references
        ssm_client.put_parameter(Name='/ref_a', Value='ssm://ref_b', Type='String')
        ssm_client.put_parameter(Name='/ref_b', Value='plain_string', Type='String')

        yield

@pytest.fixture
def mock_aws_resources_with_special_chars():
    with mock_ssm():
        ssm_client = boto3.client('ssm', region_name='us-east-1')
        ssm_client.put_parameter(Name='/test_param with spaces', Value='special_value', Type='String')
        yield

def test_load_config_with_mock_file_and_aws_resources(tmp_path, mock_aws_resources):
    # Create a mock JSON file with AWS URIs
    config_content = json.dumps({
        "ssm_value": "ssm://test_param",
        "secret_value": "secret://test_secret"
    })
    config_file = tmp_path / "config.json"
    config_file.write_text(config_content)

    aws_session = boto3.Session(region_name='us-east-1')

    # Test loading of the config file
    config = load_config(str(config_file), aws_session)
    assert config['ssm_value'] == 'test_value', "SSM value should be resolved correctly"
    assert config['secret_value'] == 'test_secret_value', "Secret value should be resolved correctly"

def test_load_yaml_config(tmp_path, mock_aws_resources):
    # Create a mock YAML file
    config_content = """
    key: value
    """
    config_file = tmp_path / "config.yaml"
    config_file.write_text(config_content)

    # Create a boto3 session
    aws_session = boto3.Session(region_name='us-east-1')

    # Test loading of the YAML config file
    config = load_config(str(config_file), aws_session)
    assert config['key'] == 'value', "YAML config should load correctly"

@mock_s3
def test_resolve_s3_uri(tmp_path, mock_aws_resources):
    # Mock S3 setup
    s3_client = boto3.client('s3', region_name='us-east-1')
    bucket_name = "test-bucket"
    object_key = "test-object"
    object_content = "s3 object content"
    s3_client.create_bucket(Bucket=bucket_name)
    s3_client.put_object(Bucket=bucket_name, Key=object_key, Body=object_content)

    # Create a mock JSON file with S3 URI
    config_content = json.dumps({"s3_value": f"s3://{bucket_name}/{object_key}"})
    config_file = tmp_path / "config.json"
    config_file.write_text(config_content)

    # Create a boto3 session
    aws_session = boto3.Session(region_name='us-east-1')

    # Test loading of the config file with the S3 URI
    config = load_config(str(config_file), aws_session)
    assert config['s3_value'] == object_content, "S3 URI should be resolved correctly"

def test_resolve_env_uri(tmp_path, mock_aws_resources):
    # Set an environment variable
    os.environ['TEST_ENV_VAR'] = 'env_var_value'

    # Create a mock JSON file with env URI
    config_content = json.dumps({"env_value": "env://TEST_ENV_VAR"})
    config_file = tmp_path / "config.json"
    config_file.write_text(config_content)

    aws_session = boto3.Session(region_name='us-east-1')

    # Test loading of the config file with the env URI
    config = load_config(str(config_file), aws_session)
    assert config['env_value'] == 'env_var_value', "env URI should be resolved correctly"

    # Clean up the environment variable
    del os.environ['TEST_ENV_VAR']

def test_resolve_json_env_uri(tmp_path, mock_aws_resources):
    # Set a JSON string in an environment variable
    os.environ['TEST_JSON_ENV_VAR'] = '{"nested_key": "nested_value"}'

    # Create a mock JSON file with json+env URI
    config_content = json.dumps({"json_env_value": "json+env://TEST_JSON_ENV_VAR"})
    config_file = tmp_path / "config.json"
    config_file.write_text(config_content)

    aws_session = boto3.Session(region_name='us-east-1')

    # Test loading of the config file with the json+env URI
    config = load_config(str(config_file), aws_session)
    assert config['json_env_value']['nested_key'] == 'nested_value', "json+env URI should be resolved correctly"

    # Clean up the environment variable
    del os.environ['TEST_JSON_ENV_VAR']

@mock_secretsmanager
def test_resolve_json_secret_uri(tmp_path, mock_aws_resources):
    # Mock Secret in Secrets Manager with JSON content
    secrets_manager_client = boto3.client('secretsmanager', region_name='us-east-1')
    secret_name = "json_test_secret"
    secrets_manager_client.create_secret(Name=secret_name, SecretString='{"json_secret_key": "json_secret_value"}')

    # Create a mock JSON file with json+secret URI
    config_content = json.dumps({"json_secret_value": f"json+secret://{secret_name}"})
    config_file = tmp_path / "config.json"
    config_file.write_text(config_content)

    aws_session = boto3.Session(region_name='us-east-1')

    # Test loading of the config file with the json+secret URI
    config = load_config(str(config_file), aws_session)
    assert config['json_secret_value']['json_secret_key'] == 'json_secret_value', "json+secret URI should be resolved correctly"

@mock_ssm
def test_resolve_json_ssm_uri(tmp_path, mock_aws_resources):
    # Mock SSM parameter with JSON content
    ssm_client = boto3.client('ssm', region_name='us-east-1')
    ssm_param_name = '/json_test_param'
    ssm_client.put_parameter(Name=ssm_param_name, Value='{"json_ssm_key": "json_ssm_value"}', Type='String')

    # Create a mock JSON file with json+ssm URI
    config_content = json.dumps({"json_ssm_value": f"json+ssm://{ssm_param_name}"})
    config_file = tmp_path / "config.json"
    config_file.write_text(config_content)

    aws_session = boto3.Session(region_name='us-east-1')

    # Test loading of the config file with the json+ssm URI
    config = load_config(str(config_file), aws_session)
    assert config['json_ssm_value']['json_ssm_key'] == 'json_ssm_value', "json+ssm URI should be resolved correctly"

@mock_s3
def test_resolve_json_s3_uri(tmp_path, mock_aws_resources):
    # Mock S3 setup with JSON content
    s3_client = boto3.client('s3', region_name='us-east-1')
    bucket_name = "json-test-bucket"
    object_key = "json-test-object"
    json_s3_content = '{"json_s3_key": "json_s3_value"}'
    s3_client.create_bucket(Bucket=bucket_name)
    s3_client.put_object(Bucket=bucket_name, Key=object_key, Body=json_s3_content)

    # Create a mock JSON file with json+s3 URI
    config_content = json.dumps({"json_s3_value": f"json+s3://{bucket_name}/{object_key}"})
    config_file = tmp_path / "config.json"
    config_file.write_text(config_content)

    aws_session = boto3.Session(region_name='us-east-1')

    # Test loading of the config file with the json+s3 URI
    config = load_config(str(config_file), aws_session)
    assert config['json_s3_value']['json_s3_key'] == 'json_s3_value', "json+s3 URI should be resolved correctly"

def test_load_unsupported_file_format(tmp_path):
    # Create a mock file with an unsupported format
    config_file = tmp_path / "config.txt"
    config_file.write_text("unsupported content")

    # Test loading of the unsupported file format
    with pytest.raises(Exception) as excinfo:
        load_config(str(config_file))
    assert "Config file must be .yml, .yaml, or .json" in str(excinfo.value), "Unsupported file format should raise an exception"

def test_load_nested_config(tmp_path, mock_aws_resources):
    # Create a nested configuration file
    config_content = """
    parent:
      child:
        ssm_value: "ssm://test_param"
        secret_value: "secret://test_secret"
    """
    config_file = tmp_path / "config.yaml"
    config_file.write_text(config_content)

    aws_session = boto3.Session(region_name='us-east-1')

    # Test loading of the nested config file
    config = load_config(str(config_file), aws_session)
    assert config['parent']['child']['ssm_value'] == 'test_value', "Nested SSM value should be resolved correctly"
    assert config['parent']['child']['secret_value'] == 'test_secret_value', "Nested secret value should be resolved correctly"

def test_resolve_nonexistent_aws_resource(tmp_path, mock_aws_resources):
    # Create a config file with a non-existent AWS resource
    config_content = """
    ssm_value: "ssm://nonexistent_param"
    """
    config_file = tmp_path / "config.yaml"
    config_file.write_text(config_content)

    aws_session = boto3.Session(region_name='us-east-1')

    # Test loading of the config file with a non-existent AWS resource
    with pytest.raises(Exception) as excinfo:
        load_config(str(config_file), aws_session)
    assert "Failed to resolve" in str(excinfo.value), "Non-existent AWS resource should raise an exception"

def test_resolve_absent_env_var(tmp_path):
    # Create a config file with an absent environment variable
    config_content = """
    env_value: "env://NONEXISTENT_ENV_VAR"
    """
    config_file = tmp_path / "config.yaml"
    config_file.write_text(config_content)

    # Test loading of the config file with an absent environment variable
    config = load_config(str(config_file))
    assert config['env_value'] == '', "Absent environment variable should return an empty string or a default value"

def test_resolve_string_literals(tmp_path):
    # Create a config file with string literals
    config_content = """
    literal_value: "some string"
    number_value: 1234
    boolean_value: true
    """
    config_file = tmp_path / "config.yaml"
    config_file.write_text(config_content)

    # Test loading of the config file
    config = load_config(str(config_file))
    assert config['literal_value'] == 'some string', "String literals should be resolved correctly"
    assert config['number_value'] == 1234, "Number literals should be resolved correctly"
    assert config['boolean_value'] is True, "Boolean literals should be resolved correctly"

def test_mixed_uri_and_literals(tmp_path, mock_aws_resources):
    # Create a config file with both URIs and literal values
    config_content = """
    uri_value: "ssm://test_param"
    literal_value: "literal string"
    number_value: 5678
    """
    config_file = tmp_path / "config.yaml"
    config_file.write_text(config_content)

    aws_session = boto3.Session(region_name='us-east-1')

    # Test loading of the config file
    config = load_config(str(config_file), aws_session)
    assert config['uri_value'] == 'test_value', "URI value should be resolved correctly"
    assert config['literal_value'] == 'literal string', "Literal string value should be preserved"
    assert config['number_value'] == 5678, "Literal number value should be preserved"

def test_partially_resolvable_config(tmp_path, mock_aws_resources):
    # Create a config file with a mix of resolvable and non-resolvable URIs
    config_content = """
    resolvable_uri: "ssm://test_param"
    non_resolvable_uri: "ssm://nonexistent_param"
    """
    config_file = tmp_path / "config.yaml"
    config_file.write_text(config_content)

    aws_session = boto3.Session(region_name='us-east-1')

    # Test loading of the config file
    with pytest.raises(Exception) as excinfo:
        load_config(str(config_file), aws_session)
    assert "Failed to resolve" in str(excinfo.value), "Non-resolvable URIs should raise an exception"

def test_invalid_json_in_aws_resources(tmp_path, mock_aws_resources_with_invalid_json):
    # Create a config file with a reference to the invalid JSON AWS resource
    config_content = """
    invalid_json_value: "json+ssm://invalid_json_param"
    """
    config_file = tmp_path / "config.yaml"
    config_file.write_text(config_content)

    aws_session = boto3.Session(region_name='us-east-1')

    # Test loading of the config file with an invalid JSON content
    with pytest.raises(Exception) as excinfo:
        load_config(str(config_file), aws_session)
    
    assert "Failed to resolve URI 'json+ssm://invalid_json_param'" in str(excinfo.value)
    assert "Expecting ':' delimiter" in str(excinfo.value), "Invalid JSON content should raise a JSON parsing error"

def test_config_with_aws_uris_without_aws_session(tmp_path):
    # Create a config file with AWS URIs
    config_content = """
    ssm_value: "ssm://test_param"
    secret_value: "secret://test_secret"
    """
    config_file = tmp_path / "config.yaml"
    config_file.write_text(config_content)

    # Test loading of the config file without providing an AWS session
    with pytest.raises(Exception) as excinfo:
        load_config(str(config_file))
    assert "Failed to resolve URI" in str(excinfo.value), "Loading AWS URIs without AWS session should raise an exception"

def test_large_config_file(tmp_path):
    # Create a large config file
    config_content = '\n'.join([f"key_{i}: value_{i}" for i in range(10000)])
    config_file = tmp_path / "config.yaml"
    config_file.write_text(config_content)

    # Test loading of the large config file
    config = load_config(str(config_file))
    assert config['key_9999'] == 'value_9999', "Large config files should be handled correctly"

def test_config_with_non_recursive_references(tmp_path, mock_non_recursive_references):
    # Create a config file with references
    config_content = """
    ref_a: "ssm://ref_a"
    ref_b: "ssm://ref_b"
    """
    config_file = tmp_path / "config.yaml"
    config_file.write_text(config_content)

    aws_session = boto3.Session(region_name='us-east-1')

    # Test loading of the config file
    config = load_config(str(config_file), aws_session)
    assert config['ref_a'] == 'ssm://ref_b', "ref_a should resolve to the URI string 'ssm://ref_b'"
    assert config['ref_b'] == 'plain_string', "ref_b should resolve to 'plain_string'"

def test_handling_incorrectly_formatted_uris(tmp_path):
    # Create a config file with incorrectly formatted URIs
    config_content = """
    malformed_uri: "ssm:/missing_part"
    """
    config_file = tmp_path / "config.yaml"
    config_file.write_text(config_content)

    # Test loading of the config file
    config = load_config(str(config_file))
    assert config['malformed_uri'] == 'ssm:/missing_part', "Incorrectly formatted URIs should be treated as string literals"

def test_config_reload_with_updated_resources(tmp_path, mock_aws_resources):
    # Create a config file referencing an AWS resource
    config_content = """
    aws_value: "ssm://test_param"
    """
    config_file = tmp_path / "config.yaml"
    config_file.write_text(config_content)

    aws_session = boto3.Session(region_name='us-east-1')

    # Load the configuration
    config = load_config(str(config_file), aws_session)
    assert config['aws_value'] == 'test_value', "Initial value should be loaded"

    # Update the AWS resource
    ssm_client = boto3.client('ssm', region_name='us-east-1')
    ssm_client.put_parameter(Name='/test_param', Value='updated_value', Type='String', Overwrite=True)

    # Reload the configuration
    config = load_config(str(config_file), aws_session)
    assert config['aws_value'] == 'updated_value', "Updated value should be loaded after reload"

def test_config_with_overlapping_keys(tmp_path):
    # Create a config file with overlapping keys
    config_content = """
    key: "first_value"
    key: "second_value"
    """
    config_file = tmp_path / "config.yaml"
    config_file.write_text(config_content)

    # Test loading of the config file
    with pytest.raises(BoxError) as excinfo:
        load_config(str(config_file))
    assert "File is not YAML as expected" in str(excinfo.value), "Config with overlapping keys should raise a BoxError"

def test_load_config_with_mock_file_uri(tmp_path):
    # Create a mock file with content
    file_content = "file content"
    file_path = tmp_path / "test_file.txt"
    file_path.write_text(file_content)

    # Create a mock JSON file with file URI
    config_content = json.dumps({"file_value": f"file://{file_path}"})
    config_file = tmp_path / "config.json"
    config_file.write_text(config_content)

    # Test loading of the config file with the file URI
    config = load_config(str(config_file))
    assert config['file_value'] == file_content, "File URI should be resolved correctly"

def test_load_config_with_mock_json_file_uri(tmp_path):
    # Create a mock JSON file with content
    json_content = {"key": "value"}
    json_file_path = tmp_path / "test_json_file.json"
    json_file_path.write_text(json.dumps(json_content))

    # Create a mock JSON file with json+file URI
    config_content = json.dumps({"json_file_value": f"json+file://{json_file_path}"})
    config_file = tmp_path / "config.json"
    config_file.write_text(config_content)

    # Test loading of the config file with the json+file URI
    config = load_config(str(config_file))
    assert config['json_file_value']['key'] == 'value', "json+file URI should be resolved correctly"

def test_load_config_with_nonexistent_file_uri(tmp_path):
    # Reference a non-existent file in the file URI
    nonexistent_file_path = tmp_path / "nonexistent_file.txt"
    config_content = json.dumps({"file_value": f"file://{nonexistent_file_path}"})
    config_file = tmp_path / "config.json"
    config_file.write_text(config_content)

    # Test loading of the config file with a non-existent file URI
    with pytest.raises(Exception) as excinfo:
        load_config(str(config_file))
    assert "Failed to resolve file at" in str(excinfo.value), "Non-existent file URI should raise an exception"

def test_load_config_with_invalid_json_file_uri(tmp_path):
    # Create a mock file with invalid JSON content
    invalid_json_content = '{"invalid_json":'
    invalid_json_file_path = tmp_path / "invalid_json_file.json"
    invalid_json_file_path.write_text(invalid_json_content)

    # Create a mock JSON file with json+file URI pointing to invalid JSON content
    config_content = json.dumps({"json_file_value": f"json+file://{invalid_json_file_path}"})
    config_file = tmp_path / "config.json"
    config_file.write_text(config_content)

    # Test loading of the config file with an invalid json+file URI
    with pytest.raises(Exception) as excinfo:
        load_config(str(config_file))
    assert "Failed to resolve URI" in str(excinfo.value), "Invalid json+file URI should raise an exception"

def test_load_config_with_empty_file_uri(tmp_path):
    # Create an empty mock file
    empty_file_path = tmp_path / "empty_file.txt"
    empty_file_path.write_text("")

    # Create a mock JSON file with file URI pointing to an empty file
    config_content = json.dumps({"file_value": f"file://{empty_file_path}"})
    config_file = tmp_path / "config.json"
    config_file.write_text(config_content)

    # Test loading of the config file with an empty file URI
    config = load_config(str(config_file))
    assert config['file_value'] == '', "Empty file URI should resolve to an empty string"

def test_load_config_with_directory_file_uri(tmp_path):
    # Reference a directory instead of a file in the file URI
    directory_path = tmp_path / "test_directory"
    directory_path.mkdir()

    config_content = json.dumps({"file_value": f"file://{directory_path}"})
    config_file = tmp_path / "config.json"
    config_file.write_text(config_content)

    # Test loading of the config file with a directory file URI
    with pytest.raises(Exception) as excinfo:
        load_config(str(config_file))
    assert "Failed to resolve file at" in str(excinfo.value), "Directory file URI should raise an exception"

def test_load_config_with_file_uri_having_special_chars(tmp_path):
    # Create a mock file with content and special characters in the filename
    file_content = "file content with special chars"
    file_name = "test_file_@#$%^&*().txt"
    file_path = tmp_path / file_name
    file_path.write_text(file_content)

    # Create a mock JSON file with file URI containing special characters
    config_content = json.dumps({"file_value": f"file://{file_path}"})
    config_file = tmp_path / "config.json"
    config_file.write_text(config_content)

    # Test loading of the config file with the file URI having special characters
    config = load_config(str(config_file))
    assert config['file_value'] == file_content, "File URI with special characters should be resolved correctly"